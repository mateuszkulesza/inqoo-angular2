import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authReq = request.clone({
      setHeaders: {
        'Authorization': 'Bearer ' + this.auth.getToken(),
        // 'X-Placki': 'Pozdrawiam Karola'
        // Access to XMLHttpRequest at 'https://api.spotify.com/v1/search?q=test&type=album' from origin 'http://localhost:4200'  from origin 'http://localhost:4200' has been blocked by CORS policy: Request header field x-placki is not allowed by Access-Control-Allow-Headers in preflight response.
      }
    })

    return next.handle(authReq).pipe(
      catchError(error => {
        if (!(error instanceof HttpErrorResponse)) {
          console.error(error);
          return throwError(new Error('Unexpected error'))
        }
        if (error.status == 401) {
          setTimeout(() => { this.auth.login() }, 2000)
          return throwError(new Error('Session expired. Logging you out'))
        }
        return throwError(new Error(error.error.error.message))
      })
    )
  }
}

// Chain of responsibility pattern
// Http.next => A.handle
// A.next = B.handle
// B.next = C.handle
// C.next = ServerHandler
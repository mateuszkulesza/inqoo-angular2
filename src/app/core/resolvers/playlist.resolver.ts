import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { Playlist } from 'src/app/playlists/model/Playlist';
import { PlaylistsService } from '../services/playlists/playlists.service';

@Injectable({
  providedIn: 'root'
})
export class PlaylistResolver implements Resolve<Playlist | null> {

  constructor(
    private service: PlaylistsService,
  ) { }

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<Playlist | null> {

    const idParamName = route.data['idParamName'] || 'id'

    const id = route.paramMap.get(idParamName)

    return id ? this.service.fetchPlaylistById(id) : of(null)
  }
}

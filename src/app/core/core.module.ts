import { Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule, HttpClientXsrfModule, HttpErrorResponse, HttpHandler, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { API_URL } from './tokens';
import { AlbumSearchService } from './services/album-search/album-search.service';
import { AuthInterceptor } from './interceptors/auth.interceptor';
import { ROUTES } from '@angular/router';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    HttpClientXsrfModule.disable()
  ],
  providers: [
    // {provide: HttpHandler, useFactory....}
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true // Cannot mix multi providers and regular providers
    },
    {
      provide: API_URL,
      useValue: environment.api_url
    },
    // AlbumSearchService
    // {
    //   provide: AbstractSearchService,
    //   useClass: SpotifyAlbumSearchService,
    //   // deps: [API_URL, HttpClient, AuthService]
    // }
    // {
    //   provide: AlbumSearchService,
    //   useFactory(api_url: string,
    //     http: HttpClient,
    //     auth: AuthService) {

    //     return new AlbumSearchService(api_url, http, auth)
    //   },
    //   deps: [API_URL, HttpClient, AuthService]
    // },
  ]
})
export class CoreModule {
  constructor(private auth: AuthService,
    @Inject(HTTP_INTERCEPTORS) interceptors: any,
    @Inject(ROUTES) routes: any) {
      // debugger
    auth.init()
  }
}

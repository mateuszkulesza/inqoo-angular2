import { Inject, Injectable } from '@angular/core';
import { Album, AlbumSearchResponse, PagingObject } from '../../model/album';
import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { Observable, from, EMPTY, throwError } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { catchError, map, pluck } from 'rxjs/operators'
import { environment } from 'src/environments/environment';
import { API_URL } from '../../tokens';
@Injectable({
  providedIn: 'root',
})
export class AlbumSearchService {

  constructor(
    @Inject(API_URL) private api_url: string,
    private http: HttpClient) { }


  getAlbumById(id: string) {
    return this.http.get<Album>(`${this.api_url}/albums/` + id, {})
  }

  getAlbums(query: string) {
    return this.http.get<AlbumSearchResponse>(`${this.api_url}/search`, {
        params: { q: query, type: 'album' },
      }).pipe(map(resp => resp.albums.items))
  }

}

import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Playlist } from 'src/app/playlists/model/Playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistsService {

  constructor() { }

  playlists: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'my favourite playlist'
  }, {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'my favourite playlist'
  }, {
    id: '345',
    name: 'Playlist 34',
    public: true,
    description: 'my favourite playlist'
  }]

  fetchPlaylists() {
    return of(this.playlists)
  }

  fetchPlaylistById(id: Playlist['id']) {
    return of(this.playlists.find(p => p.id == id)!).pipe(
      delay(Math.random() * 2000)
    )
  }

}

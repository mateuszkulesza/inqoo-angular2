import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { map, pluck, switchMap } from 'rxjs/operators';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-routed-playlist-editor',
  templateUrl: './routed-playlist-editor.component.html',
  styleUrls: ['./routed-playlist-editor.component.scss']
})
export class RoutedPlaylistEditorComponent implements OnInit {

  playlist$ = this.route.data.pipe<Playlist>(map(data => data.playlist));

  constructor(
    // private service: PlaylistsService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

  submit(form: NgForm) {
    // this.playlist$.subscribe()
    // const draft = 
    // this.service.savePlaylist(draft)
  }

}

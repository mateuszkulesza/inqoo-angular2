import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutedPlaylistDetailsComponent } from './routed-playlist-details.component';

describe('RoutedPlaylistDetailsComponent', () => {
  let component: RoutedPlaylistDetailsComponent;
  let fixture: ComponentFixture<RoutedPlaylistDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutedPlaylistDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutedPlaylistDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

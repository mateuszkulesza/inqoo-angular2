import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { iif, of } from 'rxjs';
import { map, pluck, switchMap } from 'rxjs/operators';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-routed-playlist-details',
  templateUrl: './routed-playlist-details.component.html',
  styleUrls: ['./routed-playlist-details.component.scss']
})
export class RoutedPlaylistDetailsComponent implements OnInit {

  playlist$ = this.route.data.pipe<Playlist>(pluck('playlist'));
  placki$ = this.route.data.pipe<Playlist>(pluck('placki'));

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from 'src/app/core/services/playlists/playlists.service';

@Component({
  selector: 'app-routed-playlists-list',
  templateUrl: './routed-playlists-list.component.html',
  styleUrls: ['./routed-playlists-list.component.scss']
})
export class RoutedPlaylistsListComponent implements OnInit {

  playlists$ = this.service.fetchPlaylists();

  constructor(
    private service: PlaylistsService
  ) { }

  ngOnInit(): void {
  }

}

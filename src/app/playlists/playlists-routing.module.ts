import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistResolver } from '../core/resolvers/playlist.resolver';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { RoutedPlaylistDetailsComponent } from './components/routed-playlist-details/routed-playlist-details.component';
import { RoutedPlaylistEditorComponent } from './components/routed-playlist-editor/routed-playlist-editor.component';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { RoutedPlaylistsViewComponent } from './containers/routed-playlists-view/routed-playlists-view.component';

// /user/:userId
//          /playlists/:playlistId
//                              /tracks/:trackId

const routes: Routes = [
  {
    path: 'playlists',
    // resolve: {
    //   'playlists': PlaylistsResolver
    // },
    component: RoutedPlaylistsViewComponent,
    children: [
      {
        path: '', // /playlists/
        component: RoutedPlaylistDetailsComponent
      },
      {
        path: ':id',
        data: {
          'placki': 'Awesome',
          'idParamName': 'id'
        },
        resolve: {
          'playlist': PlaylistResolver
        },
        children: [
          {
            path: '', pathMatch: 'full', redirectTo: 'details'
          },
          {
            path: 'details', // playlists/123/details
            component: RoutedPlaylistDetailsComponent,
          },
          {
            path: 'edit', // playlists/123/edit
            component: RoutedPlaylistEditorComponent
          },

        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
  ]
})
export class PlaylistsRoutingModule { }

/*

  {
    path: 'playlists',
    component: PlaylistsViewComponent,
    children: [
      // {
      //   path: 'details',
      //   component: PlaylistsComponent
      // },
      // {
      //   path: 'edit',
      //   component: PlaylistsViewComponent
      // },
    ]
  }
  // {
  //   path: 'playlists/admin',
  //   component: PlaylistsComponent
  // },
*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsViewComponent } from './containers/playlists-view/playlists-view.component';
import { PlaylistsListComponent } from './components/playlists-list/playlists-list.component';
import { PlaylistDetailsComponent } from './components/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './components/playlist-editor/playlist-editor.component';
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { FormsModule } from '@angular/forms';
import { RoutedPlaylistsViewComponent } from './containers/routed-playlists-view/routed-playlists-view.component';
import { RoutedPlaylistDetailsComponent } from './components/routed-playlist-details/routed-playlist-details.component';
import { RoutedPlaylistEditorComponent } from './components/routed-playlist-editor/routed-playlist-editor.component';
import { RoutedPlaylistsListComponent } from './components/routed-playlists-list/routed-playlists-list.component';



@NgModule({
  declarations: [
    PlaylistsViewComponent,
    PlaylistsListComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent,
    RoutedPlaylistsViewComponent,
    RoutedPlaylistDetailsComponent,
    RoutedPlaylistEditorComponent,
    RoutedPlaylistsListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    PlaylistsRoutingModule // ng g m playlists --routing -m app
  ],
  exports: [
    PlaylistsViewComponent,
  ]
})
export class PlaylistsModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-routed-playlists-view',
  templateUrl: './routed-playlists-view.component.html',
  styleUrls: ['./routed-playlists-view.component.scss']
})
export class RoutedPlaylistsViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

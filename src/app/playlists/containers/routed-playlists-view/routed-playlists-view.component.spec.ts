import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutedPlaylistsViewComponent } from './routed-playlists-view.component';

describe('RoutedPlaylistsViewComponent', () => {
  let component: RoutedPlaylistsViewComponent;
  let fixture: ComponentFixture<RoutedPlaylistsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutedPlaylistsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutedPlaylistsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

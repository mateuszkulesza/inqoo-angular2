import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlbumSearchService } from '../core/services/album-search/album-search.service';
import { AlbumDetailsViewComponent } from './containers/album-details-view/album-details-view.component';
import { AlbumSearchComponent } from './containers/album-search/album-search.component';
import { MusicComponent } from './music.component';

const routes: Routes = [{
  // app => music / ***
  path: 'music',
  component: MusicComponent,
  children: [
    {
      path: '', redirectTo: 'search', pathMatch: 'full'
    },
    {
      path: 'search',
      component: AlbumSearchComponent
    },
    {
      // path: 'albums', // /music/albums?id=5Tby0U5VndHW0SomYO7Id7
      path: 'albums/:albumId', // /music/albums/5Tby0U5VndHW0SomYO7Id7
      component: AlbumDetailsViewComponent
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicRoutingModule { }

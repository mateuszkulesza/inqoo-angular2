import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, switchMap } from 'rxjs/operators';
import { Album, Track } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';


/* 

  files:

  TODO:
  - load this on http://localhost:4200/music/albums?id=5Tby0U5VndHW0SomYO7Id7
  - show Id in html
  - fetch Alubm from server
  - show album Image, name
  - search results -> click card -> redirects here
*/

@Component({
  selector: 'app-album-details-view',
  templateUrl: './album-details-view.component.html',
  styleUrls: ['./album-details-view.component.scss']
})
export class AlbumDetailsViewComponent implements OnInit {

  albumId$ = this.route.paramMap.pipe(
    map(params => params.get('albumId'))
  )

  album$ = this.albumId$.pipe(
    filter((id): id is string => id != null),
    switchMap(id => this.service.getAlbumById(id))
  )

  @ViewChild('audioRef')
  audioRef!: ElementRef<HTMLAudioElement>

  currenTrack?: Track
  playTrack(track: Track) {
    this.currenTrack = track
    if (this.audioRef.nativeElement) {
      this.audioRef.nativeElement.src = track.preview_url
      this.audioRef.nativeElement.play()
    }
  }

  // album_id = ''
  // album?: Album

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: AlbumSearchService
  ) { }

  ngOnInit(): void {
    // this.album_id = this.route.snapshot.queryParamMap.get('id')!
    // this.album_id = this.route.snapshot.paramMap.get('albumId')!
    
    // this.service.getAlbumById(this.album_id).subscribe(album => {
    //   this.album = album;
    // })

    // manual subscribe
    // this.album$.subscribe(album => this.album = album)
  }

}

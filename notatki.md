## GIT

git clone https://bitbucket.org/mateuszkulesza/inqoo-angular2.git
cd inqoo-angular2

<!-- git remote add mojfork mojadres/do/mojfork -->

npm i

<!-- albo kopia node_modules z zipa https://we.tl/t-oSoCkSlAPA -->

npm start

## Fork

bitbucket -> (+) => fork this repositroy
git clone twojadres/twojfork twojkatlog
cd twojkatlog
git remote add trener https://bitbucket.org/mateuszkulesza/inqoo-angular2.git
git pull trener master

## Instalacje

npm install -g @angular/cli
ng --version
ng.cmd --version

Angular CLI: 12.2.7

ng new --directory . --strict

? What name would you like to use for the new workspace and initial project? inqoo-ng2
? Would you like to add Angular routing? Yes
? Which stylesheet format would you like to use? SCSS [ https://sass-lang.com/documentation/syntax#scss []

ng new --directory . --name inqoo-ng2 --strict --style scss --routing

npm i bootstrap

## Start developer server

npm start

<!-- or -->

ng serve -o

open > http://localhost:4200/

## Node modules ZIP

https://we.tl/t-oSoCkSlAPA

https://we.tl/t- oS oC kS lA PA

## Generator

npm i -g @angular/cli

ng g c clock
CREATE src/app/clock/clock.component.html (20 bytes)
CREATE src/app/clock/clock.component.spec.ts (619 bytes)
CREATE src/app/clock/clock.component.ts (272 bytes)
CREATE src/app/clock/clock.component.scss (0 bytes)
UPDATE src/app/app.module.ts (796 bytes)

## Extensions

Intelisense
https://marketplace.visualstudio.com/items?itemName=Angular.ng-template
Snippets
https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode
Switcher
https://marketplace.visualstudio.com/items?itemName=adrianwilczynski.switcher
## Playlists

LIFT 
https://angular.io/guide/styleguide#lift

ng g m playlists -m app --routing
ng g c playlists/containers/playlists-view 
ng g c playlists/components/playlists-list
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-editor

## Music Search

ng g m core -m app
ng g interface core/model/Album
ng g s core/services/AlbumSearch --flat false

ng g m music -m app --routing --route music
ng g c music/containers/album-search
  ng g c music/components/search-form
  ng g c music/components/results-grid
    ng g c music/components/album-card

